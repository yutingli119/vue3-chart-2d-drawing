# Project desctiption

Drawing on a interactive chart using Vue 3 and eCharts.js

[Preview Here](https://yutingli-vue-chart-drawing.netlify.app/)

## Install dependancy

```sh
npm install
```

### Running

```sh
npm run dev
```

### Building

```sh
npm run build
```
